import * as React from 'react'
import { times } from 'lodash'

const Quarters = ({ years, buffer, ...props }) => (
  <tr>
    <th colSpan={buffer} />
    {times(years.length, (year) => (
      <React.Fragment key={`quarters-${year}`}>
        <th key={`quarters-${year}-q1`}>Q1</th>
        <th key={`quarters-${year}-q2`}>Q2</th>
        <th key={`quarters-${year}-q3`}>Q3</th>
        <th key={`quarters-${year}-q4`}>Q4</th>
      </React.Fragment>
    ))}
  </tr>
)

export default Quarters