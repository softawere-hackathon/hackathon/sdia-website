import fetch from "node-fetch";

export default async function handler(req, res) {
  const { formId, formData } = req?.query || {};

  const url = `https://api.hsforms.com/submissions/v3/integration/secure/submit/${process.env.GATSBY_HUBSPOT_PORTAL_ID}/${formId}`

  const headers = {
    "Authorization": `Bearer ${process.env.HUBSPOT_PRIVATE_APP_TOKEN}`,
    "Content-Type": "application/json"
  }

  try {
    const result = await fetch(url, {
      method: "POST",
      body: formData,
      headers: headers,
    }).then(res => res.json())

    res.json(result)
  } catch (error) {
    res.status(500).send(error)
  }
}
