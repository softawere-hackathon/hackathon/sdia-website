import { graphql } from 'gatsby'

export const organizationMemberFragment = graphql`
  fragment PrismicOrganizationMemberFragment on PrismicOrganizationMember {
    _previewable
    uid
    url
    id
    
    data {
      headquarter
      roadmap_activities {
        activity {
          document {
            ... on PrismicRoadmapActivity {
              id
              data {
                name {
                  richText
                }
              }
            }
          }
        }
      }
      tags {
        tag
      }
      profile_image {
        gatsbyImageData(width: 800)
      }
      contact_people {
        photo {
          gatsbyImageData(width: 160)
        }
        full_name {
          richText
        }
        job_title {
          richText
        }
        contact_email
      }
      description {
        richText
      }
      name {
        richText
      }
      joined_on
      type
      link {
        url
        target
        link_type
      }
      logo {
        alt
        url
        gatsbyImageData(
          layout: FIXED
          height: 168
        )
      }
      
      body {
        ...on PrismicSliceType {
          slice_type
        }
        ...OrganizationMemberDataBodySimpleRichText
        ...OrganizationMemberDataBodyFeaturedOrgMembers
        ...OrganizationMemberDataBodyFeaturedIndividualMembers
        ...OrganizationMemberDataBodyCallToAction
      }
    }
  }
`