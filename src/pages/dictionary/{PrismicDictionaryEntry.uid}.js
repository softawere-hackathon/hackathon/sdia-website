import * as React from 'react'
import { graphql } from 'gatsby'
import { withPrismicPreview } from 'gatsby-plugin-prismic-previews'
import { RichText } from 'prismic-reactjs'

import Layout from '../../components/Layout'
import DictionaryEntryDetail from '../../components/DictionaryEntryDetail'
import SliceZone from '../../components/SliceZone'
import Seo from '../../components/Seo'

const DictionaryEntryTemplate = ({ data }) => {
  if (!data) return null
  const doc = data.prismicDictionaryEntry
  
  const {
    word,
    explanation,
  } = doc.data

  return (
    <Layout useDarkHeader={true} useDarkFooter={true}>
      <Seo title={RichText.asText(word.richText)} description={RichText.asText(explanation.richText)} />
      
      <div className="pt-12 md:pt-6">
        <DictionaryEntryDetail {...doc.data} {...doc} />
        
        {doc.body ? <SliceZone sliceZone={doc.body} /> : null}
      </div>
    </Layout>
  )
}

export const query = graphql`
  query DictionaryEntryQuery($id: String) {
    prismicDictionaryEntry(id: { eq: $id }) {
      ...PrismicDictionaryEntryFragment

      data {
        body {
          ...on PrismicSliceType {
            slice_type
          }
          ...DictionaryEntryDataBodyHubspotForm
          ...DictionaryEntryDataBodyFeaturedTeamMembers
          ...DictionaryEntryDataBodyLargeNewsletterForm
          ...DictionaryEntryDataBodySimpleRichText
          ...DictionaryEntryDataBodyFeaturedContent
          ...DictionaryEntryDataBodyFeaturedEvents
          ...DictionaryEntryDataBodyContactTeamMembers
          ...DictionaryEntryDataBodyToggleList
          ...DictionaryEntryDataBodyCallout
          ...DictionaryEntryDataBodyFeaturedOrgMembers
          ...DictionaryEntryDataBodyHubspotFormWithTeamMember
          ...DictionaryEntryDataBodyFeaturedResources
          ...DictionaryEntryDataBodyFeaturedSteeringGroups
          ...DictionaryEntryDataBodyFullQuote
          ...DictionaryEntryDataBodyCallToAction
        }
      }
    }
  }
`

export default withPrismicPreview(DictionaryEntryTemplate)
