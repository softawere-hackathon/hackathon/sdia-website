import React from 'react';
import PropTypes from 'prop-types';
import { FacebookShareButton, LinkedinShareButton, TwitterShareButton } from 'react-share';

// Assets
import FacebookIcon from '../images/icons/facebook.svg';
import LinkedInIcon from '../images/icons/linkedin.svg';
import TwitterIcon from '../images/icons/twitter.svg';


const ArticleSocialShare = ({ className, image, url }) => {
  return (
    <div className={`flex lg:flex-col justify-center gap-[12px]${className ? ` ${className}` : ''}`}>
      <FacebookShareButton
          image={image}
          url={url}
        >
          <div className="flex items-center justify-center h-[32px] w-[32px] lg:h-[40px] lg:w-[40px] bg-[#385F4B] rounded-full">
            <FacebookIcon />
          </div>
      </FacebookShareButton>
      <LinkedinShareButton
          image={image}
          url={url}
        >
          <div className="flex items-center justify-center h-[32px] w-[32px] lg:h-[40px] lg:w-[40px] bg-[#385F4B] rounded-full">
            <LinkedInIcon />
          </div>
      </LinkedinShareButton>
      <TwitterShareButton
          image={image}
          url={url}
        >
          <div className="flex items-center justify-center h-[32px] w-[32px] lg:h-[40px] lg:w-[40px] bg-[#385F4B] rounded-full">
            <TwitterIcon />
          </div>
      </TwitterShareButton>
    </div>
  );
}

ArticleSocialShare.propTypes = {
  className: PropTypes.string,
  image: PropTypes.string,
  url: PropTypes.string
}

ArticleSocialShare.defaultProps = {
  className: null,
  image: null,
  url: null
};

export default ArticleSocialShare;
