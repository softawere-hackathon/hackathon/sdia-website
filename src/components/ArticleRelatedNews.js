import * as React from 'react';
import PropTypes from 'prop-types';

import ArticleCard from './ArticleCard';

const ArticleRelatedNews = ({ className, items }) => {
  if (!Array.isArray(items) || !items.length) return null;

  // Get 3 random articles from the related articles array
  const articles = items.sort(() => Math.random() - Math.random()).slice(0, 3);

  return (
    <section className={className ? className : null}>
      <h2 className="mb-[32px]">
        Related News
      </h2>
      <div className="grid sm:grid-cols-2 lg:grid-cols-3 gap-[20px]">
        {articles.map(item => (
          <ArticleCard
            key={item.uid}
            author={item.data.author}
            categories={item.data.categories}
            mainImage={item.data.main_image}
            publicationTime={item.data.publication_time}
            slug={item.uid}
            title={item.data.title?.text}
          />
        ))}
      </div>
    </section>
  );
};

ArticleRelatedNews.propTypes = {
  className: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.shape({
    document: PropTypes.shape({
      data: PropTypes.shape({
        author: PropTypes.shape({
          document: PropTypes.shape({
            data: PropTypes.shape({
              full_name: PropTypes.shape({
                text: PropTypes.string
              }),
              photo: PropTypes.shape({
                alt: PropTypes.string,
                gatsbyImageData: PropTypes.shape({
                  height: PropTypes.number,
                  layout: PropTypes.string,
                  width: PropTypes.number
                })
              })
            })
          })
        }),
        categories: PropTypes.arrayOf(PropTypes.shape({
          category: PropTypes.shape({
            document: PropTypes.shape({
              data: PropTypes.shape({
                title: PropTypes.shape({
                  text: PropTypes.string
                })
              })
            })
          })
        })),
        excerpt: PropTypes.shape({
          text: PropTypes.string
        }),
        main_image: PropTypes.shape({
          alt: PropTypes.string
        }),
        publication_time: PropTypes.string,
        title: PropTypes.shape({
          text: PropTypes.string
        })
      })
    }),
    slug: PropTypes.string
  }))
};

ArticleRelatedNews.defaultProps = {
  className: null,
  items: null
};

export default ArticleRelatedNews
