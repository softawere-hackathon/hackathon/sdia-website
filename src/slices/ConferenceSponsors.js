import * as React from 'react';
import { graphql } from 'gatsby';
import { RichText } from 'prismic-reactjs';
import { GatsbyImage, getImage } from "gatsby-plugin-image";

import LinkButton from '/src/components/LinkButton.js';
import { linkResolver } from '../utils/LinkResolver.js';
import getCorrectTextColor from '/src/utils/SetTextColorDependingOnBackground';


const ConferenceSponsors = ({ slice }) => {
  const data = slice.primary;
  const { items } = slice;

  if (!data?.title || !Array.isArray(items) || !items.length) return null;

  const backgroundColor = slice.primary.background_color;
  const textColor = getCorrectTextColor(backgroundColor);

  const communitySponsors = items.filter(entry => entry.sponsor_type === 'Community');
  const headlineSponsors = items.filter(entry => entry.sponsor_type === 'Headline');
  const trackSponsors = items.filter(entry => entry.sponsor_type === 'Track');

  const imageElement = (item) => {
    const sponsorImage = getImage(item.sponsor_image);
    const sponsorWebsiteUrl = item.website?.url;

    return sponsorWebsiteUrl ?
      (
        <a
          href={sponsorWebsiteUrl}
          rel="noopener noreferrer"
          target="_blank"
        >
          <div className="flex items-center justify-center h-[165px] w-[165px] p-3 rounded-[12px] outline outline-1 outline-[#487662]">
            <GatsbyImage
              alt={item.sponsor_image.alt || ''}
              image={sponsorImage}
              objectFit="contain"
              objectPosition="center"
            />
          </div>
        </a>
      ) :
      (
        <div className="flex items-center justify-center h-[165px] w-[165px] p-3 rounded-[12px] outline outline-1 outline-[#487662]">
          <GatsbyImage
            alt={item.sponsor_image.alt || ''}
            image={sponsorImage}
            objectFit="contain"
            objectPosition="center"
          />
        </div>
      )
  };

  return (
    <section
      className={`relative ${backgroundColor ? 'py-[80px]' : 'my-[80px] md:my-[140px]'}`}
      style={{ backgroundColor: backgroundColor ? `${backgroundColor}` : '#FFFFFF'}}
    >
      <div className="container">
        <div className="text-center max-w-[800px] mx-auto">
          <h2
            className="font-body font-semibold text-[32px] md:text-[40px] text-[#1B202B] leading-[37px] md:leading-[44px]"
            style={{ color: textColor }}
          >
            {data.title.text}
          </h2>
          {Array.isArray(data.description?.richText) && data.description.richText[0]?.text && (
            <div
              className="text-[18px] md:text-[20px] leading-[24px] md:leading-[28px] text-[#1B202B] mx-auto mt-[20px]"
              style={{ color: textColor }}
            >
              <RichText
                linkResolver={linkResolver}
                render={data.description.richText}
              />
            </div>
          )}
        </div>
        {Array.isArray(headlineSponsors) && !!headlineSponsors.length && (
          <div className="mt-[80px]">
            <div className="flex justify-center">
              <h3
                className="font-body font-semibold text-center text-[#1B202B] text-[18px] md:text-[24px] leading-[28px] md:leading-[36px]"
                style={{ color: textColor }}
              >
                Headline Sponsors
              </h3>
            </div>
            <div className="flex flex-wrap gap-[30px] md:gap-[40px] items-center justify-center mt-[20px]">
              {headlineSponsors.map((item, index) => (
                <div
                  key={index}
                  className="relative"
                >
                  {imageElement(item)}
                </div>
              ))}
            </div>
          </div>
        )}
        {Array.isArray(communitySponsors) && !!communitySponsors.length && (
          <div className="mt-[60px]">
            <div className="flex justify-center">
              <h3
                className="font-body font-semibold text-center text-[#1B202B] text-[18px] md:text-[24px] leading-[28px] md:leading-[36px]"
                style={{ color: textColor }}
              >
                Community Sponsors
              </h3>
            </div>
            <div className="flex flex-wrap gap-[30px] md:gap-[40px] items-center justify-center mt-[20px]">
              {communitySponsors.map((item, index) => {
                 <div
                  key={index}
                  className="relative"
                >
                  {imageElement(item)}
                </div>
              })}
            </div>
          </div>
        )}
        {Array.isArray(trackSponsors) && !!trackSponsors.length && (
          <div className="mt-[60px]">
            <div className="flex justify-center">
              <h3
                className="font-body font-semibold text-center text-[#1B202B] text-[18px] md:text-[24px] leading-[28px] md:leading-[36px]"
                style={{ color: textColor }}
              >
                Track Sponsors
              </h3>
            </div>
            <div className="flex flex-wrap gap-[30px] md:gap-[40px] items-center justify-center mt-[20px]">
              {trackSponsors.map((item, index) => {
                 <div
                  key={index}
                  className="relative"
                >
                  {imageElement(item)}
                </div>
              })}
            </div>
          </div>
        )}
        {!!data?.call_to_action_link &&
        Array.isArray(data.call_to_action_text?.richText) &&
        data.call_to_action_text.richText[0]?.text && (
          <div className="flex justify-center mt-[80px]">
            <LinkButton
              className="text-[14px] md:text-[16px] leading-[18px] font-semibold text-white bg-[#E69635] py-[12px] px-[20px] rounded-[5px] hover:bg-[#dc851b]"
              isPrimary
              label={data.call_to_action_text}
              link={data.call_to_action_link}
            />
          </div>
        )}
      </div>
    </section>
  );
};

export const query = graphql`
  fragment ConferenceDataBodyConferenceSponsors on PrismicDigitalSustainabilityConferenceDataBodyConferenceSponsors {
    primary {
      title {
        text
      }
      description {
        richText
      }
      call_to_action_text {
        richText
      }
      call_to_action_link {
        url
        target
        type
        uid
        slug
        link_type
      }
      background_color
    }
    items {
      sponsor_image {
        alt
        gatsbyImageData
        url
      }
      website {
        url
      }
      sponsor_type
    }
  }
`

export default ConferenceSponsors;
