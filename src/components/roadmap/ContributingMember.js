import * as React from 'react'
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { RichText } from 'prismic-reactjs'

const ContributingMember = ({ logo, name, link }) => (
  <a href={link.url} target={link.target}>
    <GatsbyImage
      alt={RichText.asText(name.richText)}
      image={getImage(logo)}
      className="rounded shadow-lg mr-4"
    />
  </a>
)

export default ContributingMember