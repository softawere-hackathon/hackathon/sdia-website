import fetch from "node-fetch"

export default async function handler(req, res) {
  const url = "https://api.hubapi.com/marketing/v3/forms?limit=100"

  const headers = {
    "accept": "application/json",
    "authorization": `Bearer ${process.env.HUBSPOT_PRIVATE_APP_TOKEN}`
  }

  try {
    const result = await fetch(url, {
      method: "GET",
      headers: headers,
    }).then(res => res.json())

    res.json(result)
  } catch (error) {
    res.status(500).send(error)
  }
}
