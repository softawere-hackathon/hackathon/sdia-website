import { graphql } from 'gatsby';

export const articleFragment = graphql`
  fragment PrismicArticleFragment on PrismicArticle {
    id
    data {
      body {
        ...on PrismicSliceType {
          slice_type
        }
        ...ArticleDataBodyArticleTextBlock
        ...ArticleDataBodyArticleQuote
        ...ArticleDataBodyArticleImage
        ...ArticleDataBodyArticleVideo
        ...ArticleDataBodyFeaturedResources
      }
      title {
        text
        richText
      }
      author {
        document {
          ... on PrismicTeamMember {
            data {
              full_name {
                text
              }
              photo {
                gatsbyImageData
                alt
              }
            }
          }
        }
      }
      main_image {
        alt
        gatsbyImageData
      }
      publication_time
      excerpt {
        text
      }
      categories {
        category {
          document {
            ... on PrismicCategory {
              data {
                title {
                  text
                }
              }
            }
          }
          slug
        }
      }
      download_file {
        url
      }
    }
    uid
  }
`
