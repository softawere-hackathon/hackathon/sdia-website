import React from 'react'
import { times } from 'lodash'
import { RichText } from 'prismic-reactjs'
import { Link } from 'gatsby'

const Action = ({ 
  id,
  url,
  metric_uid,
  quarters,
  start_quarter,
  total_quarters,
  name,
  first,
  last,
}) => {
  return (
    <tr 
      className={`
        ${first ? 'border-t border-gray-700 border-dashed' : ''} 
        ${last ? 'border-b border-gray-700 border-dashed' : ''}
      `}
    >
      {times(start_quarter + 1, (i) => (<td key={`${id}-buffer-${i}`} />))}
      <td 
        colSpan={quarters}  
        className={`${first ? 'pt-8' : ''} ${last ? 'pb-8' : ''}`}
      >
        <h3 className="c-roadmap-item text-xl">
          <Link to={`/roadmap/${metric_uid}${url}`}>
            {RichText.asText(name.richText)}
          </Link>
        </h3>
      </td>
      
      {times(total_quarters - start_quarter - quarters, (i) => (<td key={`${id}-buffer2-${i}`} />))}
    </tr>
  )
}

export default Action