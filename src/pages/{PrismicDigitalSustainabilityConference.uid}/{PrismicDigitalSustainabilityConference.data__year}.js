import React from 'react';
import { graphql } from 'gatsby';
import { withPrismicPreview } from 'gatsby-plugin-prismic-previews';

import { Layout } from '/src/components/Layout';
import SliceZone from '/src/components/SliceZone';
import Seo from '/src/components/Seo';
import { RichText } from 'prismic-reactjs'

const DigitalSustainabilityConferenceTemplate = ({ data }) => {
  if (!data) return null;

  const { body } = data.prismicDigitalSustainabilityConference?.data || {};
  const doc = data.prismicDigitalSustainabilityConference?.data;
  const {
    page_title,
    page_description,
    share_image
  } = doc;

  let pageDescription = null;
  let pageTitle = null;
  let shareImage = share_image;

  if (page_description && Array.isArray(page_description.richText) && page_description.richText[0]?.text?.length > 0) {
    pageDescription = RichText.asText(page_description.richText);
  }

  if (page_title && Array.isArray(page_title.richText) && page_title.richText[0]?.text?.length > 0) {
    pageTitle = RichText.asText(page_title.richText);
  }

  if (share_image) shareImage = share_image;

  return (
    <Layout
      useDarkFooter={true}
      useDarkHeader={false}
    >
      <Seo
        description={pageDescription}
        image={shareImage}
        title={pageTitle || 'Digital Sustainability Conference'}
      />
      <div className="bg-white">
        {body && <SliceZone sliceZone={body} />}
      </div>
    </Layout>
  );
};

export const query = graphql`
  query DigitalSustainabilityConference($id: String) {
    prismicDigitalSustainabilityConference(id: { eq: $id }) {
      _previewable
      data {
        share_description {
          richText
        }
        share_title {
          richText
        }
        share_image {
          url
        }
        body {
          ...on PrismicSliceType {
            slice_type
          }
          ...ConferenceDataBodyConferenceHero
          ...ConferenceDataBodyConferenceTextBlock
          ...ConferenceDataBodyConferenceCountdown
          ...ConferenceDataBodyConferenceSpeakers
          ...ConferenceDataBodyConferenceQuoteWithImage
          ...ConferenceDataBodyConferenceCallToActionsAndImage
          ...ConferenceDataBodyConferenceSponsors
          ...ConferenceDataBodyConferenceTextWithCallToActionAndSideImage
          ...ConferenceDataBodyConferenceItemsWithTextAndSideImage
          ...ConferenceDataBodyToggleList
          ...ConferenceDataBodyConferenceCalendar
        }
      }
    }
  }
`;

export default withPrismicPreview(DigitalSustainabilityConferenceTemplate);
