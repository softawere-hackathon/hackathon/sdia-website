import { graphql } from 'gatsby'

export const teamMemberFragment = graphql`
  fragment PrismicTeamMemberFragment on PrismicTeamMember {
    _previewable
    id
    uid
    url

    data {
      full_name {
        richText
      }
      biography {
        richText
      }
      linkedin_profile {
        link_type
        url
        uid
        type
        target
      }
      photo {
        gatsbyImageData(
          width: 130, height: 130, imgixParams: {
            fit: "facearea",
            facepad: 3.5
            maxH: 130
          }
        )
      }
      title {
        richText
      }
    }
  }
`