import React from 'react'
import { graphql } from 'gatsby'
import { RichText } from 'prismic-reactjs'
import ChallengeDetail from './ChallengeDetail'

export const currentStateStyling = {
  'Not started': "bg-gray-300 text-gray-800",
  'Work in progress': "bg-yellow-100 text-yellow-800",
  'Raising awareness': "bg-blue-100 text-blue-800",
  'Lacking support': "bg-red-100 text-red-800",
  'Solved': "bg-green-100 text-green-800",
}

const Challenge = (props) => {
  const { 
    title,
    category,
    criticality,
    current_state,
    regional_scope,
  } = props

  return (
    <div className="xl:w-1/3 md:w-1/2 p-4">
      <div className={`
        border 
        ${current_state === 'Not started' ? 'border-gray-300 bg-gray-100' : 'border-gray-200 bg-white'} 
        p-6 rounded-lg shadow-md
      `}>
        
        <h2 className="text-lg text-gray-900 font-medium title-font mb-2">
          {RichText.asText(title.richText)}
        </h2>

        <div className='inline-flex flex-wrap gap-y-2'>
          {current_state ? 
            <span 
              className={`inline text-sm font-medium mr-2 px-2.5 py-0.5 rounded ${currentStateStyling[current_state]}`}
            >
              {current_state}
            </span>
          : null}

          {regional_scope ? 
            <span 
              className={`
                inline text-sm font-medium mr-2 px-2.5 py-0.5 rounded bg-white text-green-800 
                border border-green-800
              `}
            >
              {regional_scope}
            </span>
          : null}

          {category ? 
            <span 
              className={`
                inline text-sm font-medium mr-2 px-2.5 py-0.5 rounded bg-white text-gray-600 
                border border-gray-800
              `}
            >
              {category}
            </span>
          : null}

          {criticality ? 
            <span 
              className={`
                inline text-sm font-medium mr-2 px-2.5 py-0.5 rounded bg-white text-secondary-800 
                border border-secondary-800
              `}
            >
              {criticality}
            </span>
          : null}
        </div>
        
        <ChallengeDetail {...props} />
      </div>
    </div>
  )
}

export default Challenge

export const prismicRoadmapChallengeFragment = graphql`
  fragment PrismicRoadmapChallengeFragment on PrismicRoadmapChallenge {
    id
    uid
    data {
      title {
        richText
      }
      supporting_organizations {
        organization {
          document {
            ...PrismicOrganizationMemberFragment
          }
        }
      }
      all_indicators {
        indicator {
          richText
        }
        reference_link {
          url
          target
        }
        reference_title
      }
      applicable_region
      applicable_subregions {
        subregion
      }
      body {
        ... on PrismicSliceType {
          slice_type
        }
        ...RoadmapChallengeDataBodyFeaturedContent
        ...RoadmapChallengeDataBodyFeaturedEvents
        ...RoadmapChallengeDataBodyFeaturedResources
        ...RoadmapChallengeDataBodyFeaturedSteeringGroups
        ...RoadmapChallengeDataBodyHubspotForm
        ...RoadmapChallengeDataBodyTaggboxEmbed
      }
      category
      challenge_lead {
        document {
          ...PrismicTeamMemberFragment
        }
      }
      criticality
      current_state
      individual_supporters {
        supporter {
          document {
            ...PrismicIndividualMemberFragment
          }
        }
      }
      regional_scope
      related_activity {
        document {
          ...PrismicRoadmapActivityFragment
        }
      }
      supply_chain_layer
    }
  }
`


